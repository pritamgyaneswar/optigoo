import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {LoadingProvider} from "../../providers/loading";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
//import { StarRating } from 'ionic3-star-rating';
import {DataProvider} from '../../providers/data';
import { SocialSharing } from '@ionic-native/social-sharing';
/**
 * Generated class for the ProductDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {
  ID: any;
  images: any;
  colors: any;
  store: any;
  productName: any;
  product_mrp: any;
  product_ratings: any;
  snapdealLink;
  flipkartLink;
  amazonLink;
  amazonImg;
  flipkartImg;
  snapdealImg;
  amazon_product_price;
  amazon_product_store;
  flipkart_product_price;
  flipkart_product_store;
  snapdeal_product_price;
  snapdeal_product_store;
  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingProvider: LoadingProvider,public http: HttpClient,public sharingVar: SocialSharing, public dataProvider: DataProvider) {
    this.ID = navParams.get('ID');
   
  }
  userID;
   ionViewDidEnter() {
    this.dataProvider.getCurrentUser().subscribe(user => {
      this.userID=user.userId
      console.log(user.userId);
    });
    this.loadingProvider.show();
    this.getProductDetails('http://optigooapp.optigoo.com/index.php/search/product_details?id='+this.ID).subscribe(res => {
      this.loadingProvider.hide(); 
      this.productName = res.data.product_name;
      this.product_mrp = res.data.product_mrp;
      this.product_ratings = res.data.product_ratings;
      this.images = res.data.product_images;
      this.colors = res.data.available_colors;
      this.store = res.data.stores;
      
      //this.product_delivery=this.store[0].amazon.product_delivery;
      this.amazon_product_price=this.store[0].amazon.product_price;
      this.amazon_product_store=this.store[0].amazon.product_store;      
      this.amazonLink = this.store[0].amazon.product_store_url;
      this.amazonImg = this.store[0].amazon.product_store_logo;
      
      
      this.flipkart_product_price=this.store[1].flipkart.product_price;
      this.flipkart_product_store=this.store[1].flipkart.product_store; 
      this.flipkartLink = this.store[1].flipkart.product_store_url;
      this.flipkartImg = this.store[1].flipkart.product_store_logo;


      this.snapdeal_product_price=this.store[2].snapdeal.product_price;
      this.snapdeal_product_store=this.store[2].snapdeal.product_store; 
      this.snapdealLink = this.store[2].snapdeal.product_store_url;
      this.snapdealImg=this.store[2].snapdeal.product_store_logo;
      
      console.log(this.product_mrp);
      let tempData = {
        userId: this.userID,
        product_name: res.data.product_name,
        product_id: this.ID,
        product_mrp: res.data.product_mrp,
        product_image: res.data.product_images[0],
        product_rating: res.data.product_ratings,       
      };
      console.log(tempData);
      this.saveHistory('https://optigooapp.optigoo.com/saveHistory',tempData).subscribe(res => {
      console.log(res) 
      });
    });
    
  }
  saveHistory(url,formdata): Observable<any> {
    let JSONdata = { firebase_userid: formdata.userId, product_id: formdata.product_id, product_name: formdata.product_name, product_mrp: formdata.product_mrp,product_image:formdata.product_image,product_rating:formdata.product_rating }
      let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
      let body = JSON.stringify(JSONdata);
      console.log(body);
      return this.http
        .post(url, body, { headers: headers })
        .catch(this.handleError);
  }
 
  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
  getProductDetails(url): Observable<any> {
    return this.http
      .get(url)
      .catch(this.handleError);
  }
  compilemsg(link): string {
    var anchor = this.productName ;
    var msg = anchor + "\n Please click the link "+link+" for more details.";
    return msg.concat(" \n - Sent from optigoo !");
  }
  whatsappShare(link) {
    let msg = this.compilemsg(link);
    this.sharingVar.share(msg, null, null)
    .then(() => { },
    () => {
       
    })
    }
}
