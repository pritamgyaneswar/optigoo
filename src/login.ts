// Login Constants.
// This file contains all your Firebase settings, and app routes.
// It's important to set in your Firebase, Facebook, and Google app credentials here.
// If you have a different view for the homePage, trialPage, and verificationPage
// You can import them here and set them accordingly.
// If you want to disable emailVerification, simply set it to false.

import {TabsPage} from './pages/tabs/tabs';
import {VerificationPage} from './pages/verification/verification';
import {LoginPage} from './pages/login/login';

export namespace Login {
  export const firebaseConfig = {
    apiKey: "AIzaSyCzxvtxmSo-gM4a4Y94q07u4pwYp7k1UdE",
    authDomain: "optigoo-6fa26.firebaseapp.com",
    databaseURL: "https://optigoo-6fa26.firebaseio.com",
    projectId: "optigoo-6fa26",
    storageBucket: "optigoo-6fa26.appspot.com",
    messagingSenderId: "645888550188",
  };
  export const homePage = TabsPage;
  export const verificationPage = VerificationPage;
  export const loginpage = LoginPage;
  
  export const emailVerification: boolean = true;
}
