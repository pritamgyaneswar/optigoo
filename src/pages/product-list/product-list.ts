import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/catch';
import { LoadingProvider} from "../../providers/loading";
import { ProductDetailsPage } from '../product-details/product-details';

/**
 * Generated class for the ProductListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductListPage {
  value:any;
  productList:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: HttpClient,public loadingProvider: LoadingProvider) {
    this.value = navParams.get('item');
  }

  ionViewDidLoad() {
    this.loadingProvider.show();
    this.getProducts('https://price-api.datayuge.com/api/v1/compare/search?api_key=Y47KVB9VOYnNH6q8UKAfeHj7cb0p5zYG0aR&product='+this.value+'&sort=popularity').subscribe(res => {
    
    //this.getProducts('https://optigooapp.optigoo.com/index.php/search?name='+this.value+'&sort=popularity').subscribe(res => {
      this.loadingProvider.hide(); 
    console.log(res.data);
      this.productList=res.data;
    });
  }
  getProducts(url): Observable<any> {
      return this.http
        .get(url)
        .catch(this.handleError);
    }
    private handleError(error: any) {
      let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
      console.error(errMsg);
      return Observable.throw(errMsg);
    }
    goToDetails(productId)
    {
      this.navCtrl.push(ProductDetailsPage,{
        ID:productId
      });
    }
}
