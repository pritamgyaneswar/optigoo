import {BrowserModule} from "@angular/platform-browser";
import {ErrorHandler, NgModule} from "@angular/core";
import {IonicApp, IonicErrorHandler, IonicModule} from "ionic-angular";
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import {Camera} from "@ionic-native/camera";
import {GooglePlus} from "@ionic-native/google-plus";
import {Keyboard} from "@ionic-native/keyboard";
import {Toast} from "@ionic-native/toast";
import {MyApp} from "./app.component";
import {LoginPage} from "../pages/login/login";
import {HomePage} from "../pages/home/home";
import {VerificationPage} from "../pages/verification/verification";
import {TabsPage} from "../pages/tabs/tabs";
import {MessagesPage} from "../pages/messages/messages";
import {GroupsPage} from "../pages/groups/groups";
import {FriendsPage} from "../pages/friends/friends";
import {SearchPeoplePage} from "../pages/search-people/search-people";
import {UserInfoPage} from "../pages/user-info/user-info";
import {NewMessagePage} from "../pages/new-message/new-message";
import {MessagePage} from "../pages/message/message";
import {NewGroupPage} from "../pages/new-group/new-group";
import {GroupPage} from "../pages/group/group";
import {UpdateContactPage} from "../pages/update-contact/update-contact";
import {GroupInfoPage} from "../pages/group-info/group-info";
import {AddMembersPage} from "../pages/add-members/add-members";
import {ImageModalPage} from "../pages/image-modal/image-modal";
import {LoginProvider} from "../providers/login";
import {LogoutProvider} from "../providers/logout";
import {LoadingProvider} from "../providers/loading";
import {AlertProvider} from "../providers/alert";
import {ImageProvider} from "../providers/image";
import {DataProvider} from "../providers/data";
import {FirebaseProvider} from "../providers/firebase";
import {CountryCodeProvider} from "../providers/country-code";
import {UsersPage} from "../pages/users/users";
import {AngularFireModule} from "angularfire2";
import {AngularFireDatabaseModule} from "angularfire2/database";
import * as firebase from "firebase/app";
import {NativeAudio} from "@ionic-native/native-audio";
import {SocialSharing} from "@ionic-native/social-sharing";
import {Contacts} from "@ionic-native/contacts";
import {IonicStorageModule} from "@ionic/storage";
import {File} from "@ionic-native/file";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import { HttpClientModule } from '@angular/common/http';
import {Badge} from "@ionic-native/badge";
import {Login} from "../login";
import {FriendPipe} from "../pipes/friend";
import {SearchPipe} from "../pipes/search";
import {ConversationPipe} from "../pipes/conversation";
import {DateFormatPipe} from "../pipes/date";
import {GroupPipe} from "../pipes/group";
import { ProfilePage } from "../pages/profile/profile";
import { WalletPage } from '../pages/wallet/wallet';
import { BankListPage } from '../pages/bank-list/bank-list';
import { BankDetailPage } from "../pages/bank-detail/bank-detail";
import { SelectBankPage } from '../pages/select-bank/select-bank';
import { ProductListPage } from "../pages/product-list/product-list";
import { ProductDetailsPage } from "../pages/product-details/product-details";
import { Ionic2RatingModule } from 'ionic2-rating';
import { SearchHistoryPage } from "../pages/search-history/search-history";


firebase.initializeApp(Login.firebaseConfig);

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    VerificationPage,    
    TabsPage,
    MessagesPage,
    GroupsPage,
    FriendsPage,
    SearchPeoplePage,    
    UserInfoPage,
    NewMessagePage,
    MessagePage,
    NewGroupPage,
    GroupPage,
    GroupInfoPage,
    AddMembersPage,
    ImageModalPage,
    FriendPipe,
    ConversationPipe,
    SearchPipe,
    DateFormatPipe,
    GroupPipe,    
    UpdateContactPage,
    UsersPage,
    ProfilePage,
    WalletPage,
    BankListPage,
    BankDetailPage,
    SelectBankPage,
    ProductListPage,
    ProductDetailsPage,
    SearchHistoryPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    Ionic2RatingModule,
   
    IonicModule.forRoot(MyApp, {
      mode: "ios",
      scrollAssist: false,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(Login.firebaseConfig),
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    VerificationPage,    
    TabsPage,
    MessagesPage,
    GroupsPage,
    FriendsPage,
    SearchPeoplePage,    
    UserInfoPage,
    NewMessagePage,
    MessagePage,
    NewGroupPage,
    GroupPage,
    GroupInfoPage,
    AddMembersPage,
    ImageModalPage,    
    UpdateContactPage,    
    UsersPage,
    ProfilePage,
    WalletPage,
    BankListPage,
    BankDetailPage,
    SelectBankPage,
    ProductListPage,
    ProductDetailsPage,
    SearchHistoryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    Camera,    
    File,
    GooglePlus,
    Keyboard,
    Toast,
    CountryCodeProvider,   
    Contacts,
    Badge,
    SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LoginProvider,
    LogoutProvider,
    LoadingProvider,
    AlertProvider,
    ImageProvider,
    DataProvider,
    FirebaseProvider,
    NativeAudio
  ]
})
export class AppModule {
}
