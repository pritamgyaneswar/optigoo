import {Injectable, NgZone} from "@angular/core";
import {Facebook} from "ng2-cordova-oauth/core";
import {OauthCordova} from "ng2-cordova-oauth/platform/cordova";
import * as firebase from "firebase";
import {Login} from "../login";
import {NavController} from "ionic-angular";
import {LoadingProvider} from "./loading";
import {AlertProvider} from "./alert";
import {GooglePlus} from "@ionic-native/google-plus";
import {AngularFireDatabase} from "angularfire2/database";
import {DataProvider} from "./data";
import {asYouType, format, getPhoneCode, parse} from "libphonenumber-js";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
// import { Diagnostic } from '@ionic-native/diagnostic';

declare var cordova: any;
@Injectable()
export class LoginProvider {
  private oauth: OauthCordova;
  private navCtrl: NavController;
  // private facebookProvider = new Facebook({
  //   clientId: Login.facebookAppId,
  //   appScope: ["email"]
  // });
  phoneNumber = "";
  countryCode = "";
  constructor(
    public loadingProvider: LoadingProvider,
    public alertProvider: AlertProvider,
    public zone: NgZone,
    public googlePlus: GooglePlus,
    public angularDb: AngularFireDatabase,
    public dataProvider: DataProvider,
    public http: HttpClient
  ) {
    this.oauth = new OauthCordova();
    
  }

 
  setNavController(navCtrl) {
    this.navCtrl = navCtrl;
  }

 

  // Login on Firebase given the email and password.
  phoneLogin(email, password) {
    this.loadingProvider.show();
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(success => {
        this.loadingProvider.hide();
        this.phoneNumber = password;
        this.countryCode = "+" + getPhoneCode(parse(password).country);
        this.createUserData('','','','','','');
      })
      .catch(error => {
        let code = error["code"];
        if (code == "auth/user-not-found") {
         // this.register(email, password);
        } else {
          this.loadingProvider.hide();
          this.alertProvider.showErrorMessage(code);
        }
      });
  }

  // Login on Firebase given the email and password.
  emailLogin(email, password) {
    this.loadingProvider.show();
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(success => {
        this.loadingProvider.hide();
        this.phoneNumber = "";
        this.countryCode = "";
        this.createUserData('','','','','','');
      })
      .catch(error => {
       
        let code = error["code"];
        this.loadingProvider.hide();
        this.alertProvider.showErrorMessage(code);
        if (code == "auth/user-not-found") {
         // this.register(email, password);
        } else {
          this.loadingProvider.hide();
          this.alertProvider.showErrorMessage(code);
        }
      });
  }

  // Register user on Firebase given the email and password.
  emailRegister(email, password,firstname,lastname,phone,dob,relationshipstatus,city) {
    this.loadingProvider.show();
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(success => {
        this.loadingProvider.hide();
        this.phoneNumber = "";
        this.countryCode = "";
        this.createUserData(firstname,lastname,phone,dob,relationshipstatus,city);
      })
      .catch(error => {
        this.loadingProvider.hide();
        let code = error["code"];
        this.alertProvider.showErrorMessage(code);
      });
  }

  // Send Password Reset Email to the user.
  sendPasswordReset(email) {
    this.loadingProvider.show();
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(success => {
        this.loadingProvider.hide();
        this.alertProvider.showPasswordResetMessage(email);
      })
      .catch(error => {
        this.loadingProvider.hide();
        let code = error["code"];
        this.alertProvider.showErrorMessage(code);
      });
  }

  // Create userData on the database if it doesn't exist yet.
  createUserData(firstname,lastname,phone,dob,relationshipstatus,city) {
    firebase
      .database()
      .ref("accounts/" + firebase.auth().currentUser.uid)
      .once("value")
      .then(account => {
        // No database data yet, create user data on database
        if (!account.val()) {
          this.loadingProvider.show();
          let user = firebase.auth().currentUser;
          var userId, name, provider, img, email, phoneNumber;
          let providerData = user.providerData[0];

          userId = user.uid;

          // Get name from Firebase user.
          if (user.displayName || providerData.displayName) {
            name = user.displayName;
            name = providerData.displayName;
          } else {
            if (firstname) {
              name = firstname;
            } else {
              name = user.email;
            }
          }

          // Set default username based on name and userId.
          let username = name.replace(/ /g, "") + userId.substring(0, 8);

          // Get provider from Firebase user.
          if (providerData.providerId == "password") {
            provider = "Firebase";
          } else if (providerData.providerId == "facebook.com") {
            provider = "Facebook";
          } else if (providerData.providerId == "google.com") {
            provider = "Google";
          }

          // Get photoURL from Firebase user.
          if (user.photoURL || providerData.photoURL) {
            img = user.photoURL;
            img = providerData.photoURL;
          } else {
            img = "assets/images/profile.png";
          }

          // Get email from Firebase user.
          email = user.email;
          // Set default description.
          let description = "Hello! I am a new Communicaters user.";
          let uniqueId = Math.floor(Math.random() * 10000000000);
          let tempData = {
            userId: userId,
            name: firstname+' '+lastname,
            username: username,
            provider: provider,
            img: img,
            email: email,
            description: description,
            uniqueId: uniqueId,
            isOnline: true,
            dateCreated: new Date().toString(),
            phoneNumber: phone,
            countryCode: this.countryCode,
            DOB:dob,
            relationStatus:relationshipstatus,
            city:city,
            history:true
          };
          // Insert data on our database using AngularFire.
          this.angularDb
            .object("/accounts/" + userId)
            .set(tempData)
            .then(() => {

              this.signUp('https://optigooapp.optigoo.com/Signup',tempData).subscribe(res => {
     
    });
              this.loadingProvider.hide();
             // this.videoProvider.InitializingRTC(tempData);
              this.dataProvider.setData("userData", tempData);
              this.navCtrl.setRoot(Login.homePage, { animate: false });
            });
        } else {
          let _userData = account.val();
          if (_userData.userId) {
            this.angularDb
              .object("/accounts/" + _userData.userId)
              .update({
                isOnline: true
              })
              .then(success => {})
              .catch(error => {
                //this.alertProvider.showErrorMessage('profile/error-update-profile');
              });
          }
          console.log(account.val());
          if (!_userData.isBlock) {
          //  this.videoProvider.InitializingRTC(account.val());
            this.dataProvider.setData("userData", account.val());
            this.navCtrl.setRoot(Login.homePage, { animate: false });
          } else {
            this.alertProvider.showAlert(
              "Login Failed",
              "You are temporary block. please contact to ionSocial team "
            );
          }
        }
      });
  }
  signUp(url,formdata): Observable<any> {
    let JSONdata = { FullName: formdata.fullname, Password: formdata.password, MobileNo: formdata.phone, EmailID: formdata.email,DOB:formdata.dob,RelationshipStatus:formdata.RelationshipStatus }
      let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
      let body = JSON.stringify(JSONdata);
      //console.log(body);
      return this.http
        .post(url, body, { headers: headers })
        .catch(this.handleError);
  }
  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
