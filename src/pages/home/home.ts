import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';
import { LoadingProvider} from "../../providers/loading";
import { DataProvider} from "../../providers/data";
import { AngularFireDatabase} from "angularfire2/database";
import * as firebase from "firebase";
import * as _ from "lodash";
import { LogoutProvider} from "../../providers/logout";
import { AlertProvider} from "../../providers/alert";
import { LoginPage } from '../login/login';
import { ProductListPage } from '../product-list/product-list';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private user: any;
  public timelineData: any;
  public friendsList: any;
  isFirstTime;
  homeseg: string = "shop";
  isAndroid: boolean = false;
  userExist:any;
  value: any;
  constructor(public navCtrl: NavController,
    public loadingProvider: LoadingProvider,
    public angularDb: AngularFireDatabase,
    public dataProvider: DataProvider,public platform: Platform,
    public logoutProvider: LogoutProvider,
    public alertProvider: AlertProvider) {
    this.platform.ready().then(() => {
      this.platform.pause.subscribe(() => {
        this.isFirstTime = false;
        if (this.user.userId) {
          // Update userData on Database.
          this.angularDb
            .object("/accounts/" + this.user.userId)
            .update({
              isOnline: false
            })
            .then(success => {})
            .catch(error => {
              //this.alertProvider.showErrorMessage('profile/error-update-profile');
            });
        }
      });

      this.platform.resume.subscribe(() => {
        this.isFirstTime = false;
        if (this.user.userId) {
          // Update userData on Database.
          this.angularDb
            .object("/accounts/" + this.user.userId)
            .update({
              isOnline: true
            })
            .then(success => {})
            .catch(error => {
              //this.alertProvider.showErrorMessage('profile/error-update-profile');
            });
        }
      });

     
    });

  }
 
  onSearchByKeyword(event: any) {

   // alert('onSearchByKeyword: onSearchByKeyword invoked' + event.target.value);
    this.value = event.target.value;
   
  }
  show()
  {
    if (this.value!=null&&this.value!=undefined&&this.value!='')
    {
      this.goToProductPage(this.value); 
   }  
  }
  ionViewDidLoad() {
    this.isFirstTime = true;
    this.getTimeline();
    this.dataProvider.getCurrentUser().subscribe(user => {
      console.log("==user", user);
      if (user.isBlock) {
        this.logoutProvider.logout().then(res => {
          this.dataProvider.clearData();
         // AccountKitPlugin.logout();
          this.navCtrl.parent.parent.setRoot(LoginPage);
          this.alertProvider.showToast("You are temporary blocked.");
        });
      }
    });
  }
  getTimeline() {
    // Observe the userData on database to be used by our markup html.
    // Whenever the userData on the database is updated, it will automatically reflect on our user variable.
   // this.loadingProvider.show();
    //this.createUserData();

    let userData = this.dataProvider.getCurrentUser().subscribe(user => {
      this.user = <any>user;
      console.log("timline user", this.user);
      this.dataProvider.getContact().then(data => {
        if (data && this.user.phoneNumber != "") {
          this.dataProvider.setContactWithCountryCode(this.user.countryCode);
        }
      });
     
      userData.unsubscribe();

      //  Update userData on Database.
    });
  
      this.dataProvider.setData("userData", this.user);
      localStorage.setItem('userlist',JSON.stringify(this.user));
    console.log('userlist:',JSON.stringify(this.user));
    
    console.log('localstorage userlist:',localStorage.getItem('userlist'));
    // Get Friend  List
    this.dataProvider.getFriends().subscribe(friends => {
      // Get timeline by user
      this.dataProvider.getTimelinePost().subscribe(post => {
        this.loadingProvider.hide();

        if (this.timelineData) {
          let timeline = post[post.length - 1];
          let tempData = <any>{};
          tempData = timeline;

          let friendIndex = _.findKey(friends, data => {
            let _tempData = <any>data;
            return _tempData.$value == timeline.postBy;
          });
          if (
            friendIndex ||
            timeline.postBy == firebase.auth().currentUser.uid
          ) {
            this.dataProvider.getUser(timeline.postBy).subscribe(user => {
              tempData.avatar = user.img;
              tempData.name = user.name;
            });

            // Check Locaion

            if (timeline.location) {
              let tempLocaion = JSON.parse(timeline.location);
              tempData.lat = tempLocaion.lat;
              tempData.long = tempLocaion.long;
              tempData.location =
                "https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=500x200&maptype=roadmap&markers=color:red|label:S|40.702147,-74.015794&key=AIzaSyAt0edUAx4S2d7z8wh1Xe04yE9Xml1ZLPY";
            }

            //  ===== check like and commnets ===
            this.dataProvider.getLike(tempData.$key).subscribe(likes => {
              tempData.likes = likes.length;

              let isLike = _.findKey(likes, like => {
                let _tempLike = <any>like;
                return _tempLike.$value == firebase.auth().currentUser.uid;
              });

              if (isLike) {
                tempData.isLike = true;
              } else {
                tempData.isLike = false;
              }
            });

            //  ===== check dilike
            this.dataProvider.getdisLike(tempData.$key).subscribe(dislikes => {
              tempData.dislikes = dislikes.length;
              // Check post like or not

              let isdisLike = _.findKey(dislikes, dislike => {
                let _tempLike = <any>dislike;
                return _tempLike.$value == firebase.auth().currentUser.uid;
              });

              if (isdisLike) {
                tempData.isdisLike = true;
              } else {
                tempData.isdisLike = false;
              }
            });

            //  ===== check commnets
            this.dataProvider.getComments(tempData.$key).subscribe(comments => {
              tempData.comments = comments.length;
              // Check post like or not

              let isComments = _.findKey(comments, comment => {
                let _tempComment = <any>comment;
                return (
                  _tempComment.commentBy == firebase.auth().currentUser.uid
                );
              });

              if (isComments) {
                tempData.isComment = true;
              } else {
                tempData.isComment = false;
              }
            });

            // this.addOrUpdateTimeline(tempData)
            this.timelineData.unshift(tempData);
          }
        } else {
          this.timelineData = [];
          post.forEach(data => {
            this.dataProvider.getTimeline(data.$key).subscribe(timeline => {
              if (timeline.$exists()) {
                let tempData = <any>{};
                tempData = timeline;
                let friendIndex = _.findKey(friends, data => {
                  let _tempData = <any>data;
                  return _tempData.$value == timeline.postBy;
                });
                if (
                  friendIndex ||
                  timeline.postBy == firebase.auth().currentUser.uid
                ) {
                  this.dataProvider.getUser(timeline.postBy).subscribe(user => {
                    tempData.avatar = user.img;
                    tempData.name = user.name;
                  });

                  // Check Location
                  if (timeline.location) {
                    let tempLocaion = JSON.parse(timeline.location);
                    tempData.lat = tempLocaion.lat;
                    tempData.long = tempLocaion.long;
                    tempData.location =
                      "https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=500x300&maptype=roadmap&markers=color:red|label:S|" +
                      tempLocaion.lat +
                      "," +
                      tempLocaion.long;
                  }

                  //  ===== check like
                  this.dataProvider.getLike(tempData.$key).subscribe(likes => {
                    tempData.likes = likes.length;
                    // Check post like or not

                    let isLike = _.findKey(likes, like => {
                      let _tempLike = <any>like;
                      return (
                        _tempLike.$value == firebase.auth().currentUser.uid
                      );
                    });

                    if (isLike) {
                      tempData.isLike = true;
                    } else {
                      tempData.isLike = false;
                    }
                  });

                  //  ===== check dilike
                  this.dataProvider
                    .getdisLike(tempData.$key)
                    .subscribe(dislikes => {
                      tempData.dislikes = dislikes.length;
                      // Check post like or not

                      let isdisLike = _.findKey(dislikes, dislike => {
                        let _tempLike = <any>dislike;
                        return (
                          _tempLike.$value == firebase.auth().currentUser.uid
                        );
                      });

                      if (isdisLike) {
                        tempData.isdisLike = true;
                      } else {
                        tempData.isdisLike = false;
                      }
                    });

                  //  ===== check commnets
                  this.dataProvider
                    .getComments(tempData.$key)
                    .subscribe(comments => {
                      tempData.comments = comments.length;
                      // Check post like or not

                      let isComments = _.findKey(comments, comment => {
                        let _tempComment = <any>comment;
                        return (
                          _tempComment.commentBy ==
                          firebase.auth().currentUser.uid
                        );
                      });

                      if (isComments) {
                        tempData.isComment = true;
                      } else {
                        tempData.isComment = false;
                      }
                    });

                  // this.addOrUpdateTimeline(tempData)
                  this.timelineData.unshift(tempData);
                }
              }
            });
          });
        }
      });
    });
   
  }
  goToProductPage(productCategory)
  {
    console.log(productCategory);
    this.navCtrl.push(ProductListPage,{
      item:productCategory
    });
  }

}
