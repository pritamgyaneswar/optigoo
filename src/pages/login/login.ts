import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {LoginProvider} from '../../providers/login';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Validator} from '../../validator';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public mode: string;
  private emailPasswordForm: FormGroup;
  private emailForm: FormGroup;
  private registerForm:FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  
  constructor(public navCtrl: NavController, 
    public loginProvider: LoginProvider, 
    
    public formBuilder: FormBuilder) {
    
    this.loginProvider.setNavController(this.navCtrl);
    // Create our forms and their validators based on validators set on validator.ts.
    this.emailPasswordForm = formBuilder.group({
      email: Validator.emailValidator,
      password: Validator.passwordValidator
    });
    this.registerForm = formBuilder.group({
      email: Validator.emailValidator,
      password: Validator.passwordValidator,
      firstName:'',
      lastName:'',
      phone:'',
      dob:'',
      relationshipStatus:'',
      city:''
    });
    
  }

  ionViewDidLoad() {
   
    this.mode = 'main';
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
}



  // Call loginProvider and register the user with email and password.
  register() {    
    this.loginProvider.emailRegister(this.registerForm.value["email"], this.registerForm.value["password"],
    this.registerForm.value["firstName"], this.registerForm.value["lastName"],this.registerForm.value["phone"],
    this.registerForm.value["dob"], this.registerForm.value["relationshipStatus"],this.registerForm.value["city"]
    );    
  }
 
  login(){
      this.loginProvider.emailLogin(this.emailPasswordForm.value["email"], this.emailPasswordForm.value["password"]);
  }

  // Call loginProvider and send a password reset email.
  forgotPassword() {
    this.loginProvider.sendPasswordReset(this.emailForm.value["email"]);
    
  }

  
  

}
