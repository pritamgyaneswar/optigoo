import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import {DataProvider} from "../../providers/data";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the BankDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-bank-detail',
  templateUrl: 'bank-detail.html',
})
export class BankDetailPage {
editForm:any={};
private user: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public dataProvider: DataProvider,public http: HttpClient) {
      this.editForm.bankName=localStorage.getItem('BankName');
      this.editForm.bankHolderName=localStorage.getItem('BankHolderName');

      this.editForm.accountType=localStorage.getItem('AccountType');
      this.editForm.accountNumber=localStorage.getItem('AccountNumber');
      this.editForm.IFSCCode=localStorage.getItem('IFSCCode');
  }

  ionViewDidLoad() {
    this.dataProvider.getCurrentUser().subscribe(user => {
      // this.loadingProvider.hide();

      this.user = <any>user;
    });
    console.log('ionViewDidLoad BankDetailPage');
  }
  closeModal()
  {
    this.navCtrl.pop();
  }
  addBank()
  {
    this.saveBank('https://optigooapp.optigoo.com/postBankDetails',this.editForm).subscribe(res => {
      console.log(res) ;
      this.navCtrl.pop();
      });
    // localStorage.setItem('BankName',this.editForm.bankName);
    //         localStorage.setItem('BankHolderName',this.editForm.bankHolderName);
    //         localStorage.setItem('AccountType',this.editForm.accountType);
    //         localStorage.setItem('AccountNumber',this.editForm.accountNumber);
    //         localStorage.setItem('IFSCCode',this.editForm.IFSCCode);
           
  }
  saveBank(url,formdata): Observable<any> {
    let JSONdata = {
      "UserID":this.user.userId,
      "firebase_userid":this.user.userId,
      "BankID":9,
      "BankName":this.editForm.bankName,
      "BankHolderName":this.editForm.bankHolderName,
      "AccountType":this.editForm.accountType,
      "AccountNumber":this.editForm.accountNumber,
      "IFSCCode":this.editForm.IFSCCode
      }
      let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
      let body = JSON.stringify(JSONdata);
      console.log(body);
      return this.http
        .post(url, body, { headers: headers })
        .catch(this.handleError);
  }
  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
