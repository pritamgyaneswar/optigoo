import {Component} from "@angular/core";
import {AlertController, App, ModalController, NavController, NavParams} from "ionic-angular";
import {LogoutProvider} from "../../providers/logout";
import {LoadingProvider} from "../../providers/loading";
import {AlertProvider} from "../../providers/alert";
import {ImageProvider} from "../../providers/image";
import {DataProvider} from "../../providers/data";
import {AngularFireDatabase} from "angularfire2/database";
import {Camera} from "@ionic-native/camera";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ProductDetailsPage } from "../product-details/product-details";

/**
 * Generated class for the SearchHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-search-history',
  templateUrl: 'search-history.html',
})
export class SearchHistoryPage {
  productList:any;
  value:any;
  user:any;
  ID:any;
  searchID:any;
  constructor( public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public app: App,
    public logoutProvider: LogoutProvider,
    public modalCtrl: ModalController,
    public loadingProvider: LoadingProvider,
    public imageProvider: ImageProvider,
    public angularDb: AngularFireDatabase,
    public alertProvider: AlertProvider,
    public dataProvider: DataProvider,
    public camera: Camera,
    public http: HttpClient) {
      this.ID = navParams.get('ID');
  }

  ionViewDidLoad() {

    this.getUser();
  }
  getUser()
  {
    this.dataProvider.getCurrentUser().subscribe(user => {
      // this.loadingProvider.hide();
   
      
      this.user = <any>user;
      console.log(" user", this.user);
      if(this.ID!=''&&this.ID!=null&&this.ID!=undefined)
      {
        this.searchID= this.ID;
      }
      else
      {
        
        this.searchID= this.user.userId;
        
      }
      
      this.getHistory('https://optigooapp.optigoo.com/getHistory',this.searchID).subscribe(res => {
    
     this.productList=res[0].SearchList;
     console.log(this.productList) ; 
      });
    });
  }
  getHistory(url,userid): Observable<any> {
    let JSONdata = { firebase_userid: userid}
      let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
      let body = JSON.stringify(JSONdata);
      console.log(body);
      return this.http
        .post(url, body, { headers: headers })
        .catch(this.handleError);
  }
  
 
  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
  goToDetails(productId)
    {
      this.navCtrl.push(ProductDetailsPage,{
        ID:productId
      });
    }
}
