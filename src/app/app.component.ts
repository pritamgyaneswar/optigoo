import {Component,ViewChild} from '@angular/core';
import {Events, ModalController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';
import {AngularFireDatabase} from 'angularfire2/database';
//Pages
import {LoginPage} from '../pages/login/login';
import {TabsPage} from '../pages/tabs/tabs';
import {DataProvider} from '../providers/data';

import * as firebase from "firebase";
import {Nav} from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  userData: any;
  private user: any;
  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    keyboard: Keyboard,
    public modalCtrl:ModalController,
    public events:Events,
    public angularDb:AngularFireDatabase,
    public dataProvider: DataProvider
    //private backgroundMode: BackgroundMode
    ) {
      
    platform.ready().then(() => {
      let that = this;
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {         
         // alert("Signed in user!")
          if (user.uid) {
            that.nav.setRoot(TabsPage);          
              let userData = that.dataProvider.getCurrentUser().subscribe(user => {
                that.user = <any>user;
                console.log("timline user", that.user);               
                that.dataProvider.setData("userData", that.user);
                userData.unsubscribe();          
                //  Update userData on Database.
              });
          }        
         
        } else {
        //  alert("No user!")
          that.rootPage = LoginPage;
        }
      });
      statusBar.overlaysWebView(false);
      statusBar.backgroundColorByHexString('#CCCCCC');
      statusBar.styleLightContent();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      console.log('userlist:',localStorage.getItem("userList"));
    });
  }
 
}
